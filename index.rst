.. Micromorph documentation master file, created by
   sphinx-quickstart on Fri Aug 14 21:27:31 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Micromorph
==========

The `PSAAP <https://www.energy.gov/nnsa/articles/nnsa-announces-selection-predictive-science-academic-alliance-program-centers>`_ Multi-disciplinary Simulation Center (MSC) for **Micromorphic Multiphysics Porous and
Particulate Materials Simulations Within Exascale Computing Workflows** seeks to advance
the state of multiscale predictive science for porous composite inelastic materials
through grain-resolving direct numerical simulation, calibration and validation through
unique experiments with advanced imaging, and numerical upscaling to create fast-running
analysis and design tools.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview
   team
   theory
   num-methods
   software
   verification
   experiments
   val-uq
   workflows
   zreferences
   publications

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
