Team
====

Co-directors
------------

* `Jed Brown <https://jedbrown.org>`_ (exascale software and hardware, ML, workflows), Assistant Professor, CU Boulder
* `Amy Clarke <https://metallurgy.mines.edu/project/clarke-amy/>`_ (experiments), Associate Professor, Colorado School of Mines
* `Alireza Doostan <https://www.colorado.edu/aerospace/alireza-doostan>`_ (V&V/UQ), Associate Professor, CU Boulder
* `Richard Regueiro (PI) <https://ceae.colorado.edu/~regueiro/>`_ (computational multiscale multiphysics modeling), Professor, CU Boulder
* `Henry Tufo <https://www.colorado.edu/cs/henry-tufo>`_ (exascale software and hardware), Professor, CU Boulder

Senior Personnel
----------------

* `Khalid Alshibli <http://web.utk.edu/~alshibli/personal.php>`_ (experiments), Professor, University of Tennessee Knoxville
* `Kester Clarke <https://metallurgy.mines.edu/project/clarke-kester/>`_ (experiments), Assistant Professor, Colorado School of Mines
* `Ken Jansen <https://www.colorado.edu/aerospace/kenneth-jansen>`_ (exascale, software), Professor, CU Boulder
* `Shelley Knuth <https://www.colorado.edu/rc/about/knuth>`_ (software, training), Associate Director, Research Computing, CU Boulder
* `Christian Linder <https://profiles.stanford.edu/christian-linder>`_ (multiscale), Associate Professor, Stanford University
* `Hongbing Lu <https://me.utdallas.edu/faculty-staff/hongbing-lu/>`_ (experiments), Professor, University of Texas at Dallas
* `Ron Pak <https://www.colorado.edu/ceae/ronald-y-s-pak>`_ (verification), Professor, CU Boulder
* `Fatemeh Pourahmadian <https://www.colorado.edu/ceae/fatemeh-pourahmadian>`_ (experiments), Assistant Professor, CU Boulder
* `J-H Song <https://www.colorado.edu/faculty/song/>`_ (multiscale), Assistant Professor, CU Boulder
* `Steve Waiching Sun <https://engineering.columbia.edu/faculty/steve-waiching-sun>`_ (multiscale), Associate Professor, Columbia University
* `Franck Vernerey <https://www.colorado.edu/lab/vernerey/>`_ (multiscale), Professor, CU Boulder
* `Beichuan Yan <https://www.researchgate.net/profile/Beichuan-Yan>`_ (exascale, software, DNS), Research Software Engineer, CU Boulder, beichuan.yan@colorado.edu, ParaEllip3d-CFD code
* `Yida Zhang <https://sites.google.com/view/yida-group/home>`_ (experiments), Assistant Professor, CU Boulder

Post-doctoral Researchers
-------------------------

* `Matthias Neuner <https://www.researchgate.net/profile/Matthias_Neuner>`_, University of Innsbruck, matthias.neuner@uibk.ac.at, gradient damage-plasticity within micromorphic continuum
* Yao Ren, UT Dallas, yao.ren@utdallas.edu, in-situ CT imaging, high rate (SHPB, gas gun) and quasi-static experiments
* Jacqui Wentz, CU Boulder, jawe2262@colorado.edu, UQ
* `Qing Yin <http://web.stanford.edu/~qingyin/>`_, Columbia, qy2259@columbia.edu, postdoc advisor: Prof. Steve WaiChing Sun, ML for DNS-to-micromorphic upscaled constitutive equations, multiscale modeling, viscoplasticity
Graduate students
-----------------

* Thomas Allard, CU Boulder, thomas.allard@colorado.edu, apply DNS-to-micromorphic upscaling framework in MOOSE/Tardigrade to mock HE and simpler bonded particulate materials
* Prajwal Kammardi Arunachala, Stanford, prajwalk@stanford.edu, 3D macroscale fracture via EFEM and phase field in MOOSE/Tardigrade
* Gus Becker, Mines, chbecker@mymail.mines.edu, in-situ CT imaging and segmentation
* Ning Bian, UT Dallas, in-situ CT imaging, high rate (SHPB, gas gun) and quasi-static experiments
* Summer Camerlo, Mines, scamerlo@mymail.mines.edu, first year Master's student under Dr. Amy Clarke (Materials Science, expected graduation May 2022), research area: characterization of mock HE, technical area: bonded particles sample preparation (glass beads, aluminum particles, sand, epoxy, mock HE)
* Kieran Fung, CU Boulder, kieran.fung@colorado.edu, statistical interpretation of particle DNS for statistical micromorphic theory
* Zach Irwin, CU Boulder, zachariah.irwin@colorado.edu, DNS simulations
* Zaher Jarrar, U Tennessee Knoxville, zjarrar@vols.utk.edu, single and few particle in-situ quasi-static and high rate compression experiments, CT imaging and segmentation
* Nathan Miller, CU Boulder, nathan.a.miller@colorado.edu, upscaling framework for DNS-to-micromorphic continuum, MOOSE/Tardigrade code
* Tim Ngo, Stanford, ngotm@stanford.edu, 3D macroscale fracture via EFEM and phase field in MOOSE/Tardigrade
* Jarett Poliner, Columbia, jsp2195@columbia.edu, ML for DNS-to-micromorphic upscaled constitutive equations
* Peter Schaefferkoetter, CU Boulder, peter.schaefferkoetter@colorado.edu, DNS-to-micromorphic upscaled continuum fracture within MOOSE/Tardigrade
* Karen (Ren) Stengel, CU Boulder, karen.stengel@colorado.edu, exascale software
* Jeremy Thompson, CU Boulder, jeremy.thompson@colorado.edu, exascale software
* Andrea Tyrrell, CU Boulder, andrea.tyrrell@colorado.edu, high-pressure thermal triaxial compression experiments on mock HE and other bonded particulate material samples
* Nikolaos Vlassis, Columbia, nnv2102@columbia.edu, ML for DNS-to-micromorphic upscaled constitutive equations
* Zeyu Xiong, Columbia, zx2312@columbia.edu, ML for DNS-to-micromorphic upscaled constitutive equations
* Runyu Zhang, UT Dallas, runyu.zhang2@utdallas.edu, in-situ CT imaging, high rate (SHPB, gas gun) and quasi-static experiments

Undergraduate students
----------------------

* Vijay Kulkarni, UT Dallas, vnk170000@utdallas.edu, assist with in-situ CT imaging, high rate (SHPB, gas gun) and quasi-static experiments

